package com.example.helloworld;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class newController {
    @GetMapping
    public String getData(){
        return "Hello World !!!!";
    }

    @GetMapping("/testrequestparam")
    public String getTestrequestParam(@RequestParam String id )
    {
        return "Test Request Param "+id;

    }

    @GetMapping("/testPathparam/{id}")
    public String getPathParam(@PathVariable("id") String id)
    {
        return "test Path param ="+id;
    }

  
    
}
